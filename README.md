
# LIS 4368

## Alexis Marrero

1. [Assignment 1](a1/README.md "My A1 README.md file")
    * Instal Apache
    * Instal JDK
    * Instal Bitbucket repo
    * Create Bitbucket Tutorials 
	* Provide git command descriptions
    
2. [Assignment 2](https://bitbucket.org/acm14n/lis4368_2/src/d53c9ef4e5c089ee7ee88ab431d10a71fc7f8cba/a2/README.MD?at=master&fileviewer=file-view-default)
    * Create a data driven website design
    * Compile Servlet files
    * Create local host web pages
    * Provide screenshotss
    
3. [Assignment 3](a3/README.md "A3 README.md file")

    * Create a database solution that interacts with a webapp
    * Forward Engineer SQL
    * Create valid data
    
    
4. [Project 1](p1/README.md "P1 README.md file")

    * Client side validation for server
    * Data validation
    * Creating interactive database

5. [Assignment 4](a4/README.md "A4 README.md file")

	* Create index.php file
	* Add jQuery validation expressions per entity attribute requirements
	* Carousel with responsive images using boostrap

6. [Assignment 5](a5/README.md "A5 README.md file")

    * Server-side validation
    * SQL forward engineering  
    * Create error.php

7. [Project 2](p2/README.md "P2 README.md file")
    
    * Server-side validation
    * Edit and delete functionalities  
    * connection to database